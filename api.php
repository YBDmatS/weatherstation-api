<?php

function getConnexion()
{
    $mysqlHost = "localhost";
    $dbName = "WeatherStation";
    $dbUserName = "root";
    $dbPassword = "";
    $pdo = new PDO("mysql:host=$mysqlHost;dbname=$dbName", $dbUserName, $dbPassword);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $pdo->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
    return $pdo;
}

function sendJson($infos)
{
    header("Access-Control-Allow-Origin:*");
    header("Content-type:application/json");
    $infos = ReplaceStringByFloat($infos);
    echo json_encode($infos, JSON_UNESCAPED_UNICODE);
}

function ReplaceStringByFloat($array) {
    foreach ($array as $key => $oneLine) {
        if (isset($oneLine["Temperature"])) {
            $oneLine["Temperature"] = floatval($oneLine["Temperature"]);
        }
        if (isset($oneLine["Humidity"])) {
            $oneLine["Humidity"] = floatval($oneLine["Humidity"]);
        }
        $array[$key] = $oneLine;
    }
    return $array;
}

function getreports()
{
    $pdo = getConnexion();
    $req = "SELECT r.Date_Time , r.Temperature, r.Humidity, p.Probe_Name  from reports r inner join Probes p on r.Probe_ID = p.Probe_MacAddress";
    $rslt = $pdo->query($req);
    $reports = $rslt->fetchAll(PDO::FETCH_ASSOC);
    sendJson($reports);
}

function getDailyReports()
{
    $pdo = getConnexion();
    $req = "SELECT r.Date_Time , r.Temperature, r.Humidity, p.Probe_Name  from reports r inner join Probes p on r.Probe_ID = p.Probe_MacAddress where date(r.Date_Time) = date(now()) order by r.Date_Time desc limit 11";
    $rslt = $pdo->query($req);
    $reports = $rslt->fetchAll(PDO::FETCH_ASSOC);
    sendJson($reports);
}

function postReport()
{
    $jsonInput = json_decode(file_get_contents("php://input"));
    $temperature = $jsonInput->Temperature;
    $humidity = $jsonInput->Humidity;
    $ProbeID = $jsonInput->Probe_ID;

    $pdo = getConnexion();

    CheckOrInsertNewProbe($pdo, $ProbeID);

    $sql = "INSERT INTO reports (Temperature, Humidity, Probe_ID, Date_Time ) VALUES (?,?,?, NOW())";
    $stmt = $pdo->prepare($sql);
    $success = $stmt->execute([$temperature, $humidity, $ProbeID]);

    $message = [
        "message" => "insert ok",
        "code" => 200
    ];

    if ($success === false) {
        $message = [
            "message" => "insert ko:" . implode($stmt->errorInfo()),
            "code" => 500
        ];
    }
    sendJson($message);
}

function CheckOrInsertNewProbe($pdo, $Probe)
{
    $selectSQL = "SELECT * FROM Probes where Probe_MacAddress = :addr";
    $selectStmt = $pdo->prepare($selectSQL);
    $selectStmt->bindParam('addr', $Probe);
    $selectStmt->execute();
    $Probes = $selectStmt->fetchAll();

    if (count($Probes) == 0) {
        $insertSQL = "INSERT INTO Probes (Probe_MacAddress , Probe_Name , Probe_Location ) VALUES (?,?,?)";
        $insertStmt = $pdo->prepare($insertSQL);
        $insertStmt->execute([$Probe, $Probe, $Probe]);
    }
}

function getreportsByProbe($Probe)
{
    $pdo = getConnexion();
    $req = "SELECT r.Date_Time , r.Temperature, r.Humidity, p.Probe_Name  from reports r inner join Probes p on r.Probe_ID = p.Probe_MacAddress where Probe_Name = ?";
    $stmt = $pdo->prepare($req);
    $stmt->execute([$Probe]);
    $reportsForOneProbe = $stmt->fetchAll(PDO::FETCH_ASSOC);
    sendJson($reportsForOneProbe);
}

function getProbes()
{
    echo "get Probes";
}