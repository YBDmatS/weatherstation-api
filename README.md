# [[ WeatherStation - API ]]



## Description

Developed for a Weather Station student project.

It's an API to send humidity and temperature report from ESP01 to a MariaDB Database and show data on a Website.

The database and the website are on raspberry pi zero with a LAMP environment.

![Schema](Schema.png)

To see the whole project, have a look to the 2 other repositories :

https://gitlab.com/YBDmatS/weatherstation-esp01

https://gitlab.com/YBDmatS/weatherstation-site




## Database Tables

```sql
DESC Probes;
+------------------+-------------+------+-----+---------+-------+
| Field            | Type        | Null | Key | Default | Extra |
+------------------+-------------+------+-----+---------+-------+
| Probe_Name       | varchar(45) | NO   |     | NULL    |       |
| Probe_Location   | varchar(45) | NO   |     | NULL    |       |
| Probe_MacAddress | varchar(45) | NO   | PRI | NULL    |       |
+------------------+-------------+------+-----+---------+-------+
```

```mysql
DESC Reports;
+-------------+---------------+------+-----+---------+-------+
| Field       | Type          | Null | Key | Default | Extra |
+-------------+---------------+------+-----+---------+-------+
| Date_Time   | datetime      | NO   | PRI | NULL    |       |
| Temperature | decimal(10,2) | NO   |     | NULL    |       |
| Humidity    | decimal(10,2) | NO   |     | NULL    |       |
| Probe_ID    | varchar(45)   | NO   | MUL | NULL    |       |
+-------------+---------------+------+-----+---------+-------+
```



## API documentation



![Weather_API](Weather_API.png)

